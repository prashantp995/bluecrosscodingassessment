import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CeilingFanTest {

    private CeilingFan ceilingFan;
    private static final String FAILURE_MSG_FAN_IS_NOT_OFF = "Fan is not off";
    private static final String FAILURE_MSG_FWD_DIRECTION = "Fan direction is not forward";
    private static final String FAILURE_MSG_REV_DIRECTION = "Fan direction is not reverse";
    private static final String FAILURE_MSG_EXPECTED_SPEED = "Fan is not running at expected speed %d";


    @Before
    public void initFan() {
        ceilingFan = new CeilingFan();
        assertNotNull("New CeilingFan fan is not initiated", ceilingFan);
        assertEquals(FAILURE_MSG_FAN_IS_NOT_OFF, CeilingFan.OFF, ceilingFan.getSpeed());
        assertEquals(FAILURE_MSG_FWD_DIRECTION, CeilingFan.FORWARD, ceilingFan.getDirection());
    }

    @Test
    public void testForward() {
        changeSpeed();
        assertEquals(getFailMsgForSpeed(1), 1, ceilingFan.getSpeed());
        changeSpeed();
        assertEquals(getFailMsgForSpeed(2), 2, ceilingFan.getSpeed());
        changeSpeed();
        assertEquals(getFailMsgForSpeed(3), 3, ceilingFan.getSpeed());
        changeSpeed();
        assertEquals(FAILURE_MSG_FAN_IS_NOT_OFF, CeilingFan.OFF, ceilingFan.getSpeed());
        changeSpeed();
        assertEquals(getFailMsgForSpeed(1), 1, ceilingFan.getSpeed());
    }

    @Test
    public void testReverse() {
        changeDirection();
        assertEquals(FAILURE_MSG_REV_DIRECTION, CeilingFan.REVERSE, ceilingFan.getDirection());
        changeSpeed();
        assertEquals(getFailMsgForSpeed(1), 1, ceilingFan.getSpeed());
        assertEquals(FAILURE_MSG_REV_DIRECTION, CeilingFan.REVERSE, ceilingFan.getDirection());
        changeSpeed();
        assertEquals(getFailMsgForSpeed(2), 2, ceilingFan.getSpeed());
    }

    @Test
    public void testFwdAndRev() {
        changeDirection();
        assertEquals(FAILURE_MSG_REV_DIRECTION, CeilingFan.REVERSE, ceilingFan.getDirection());
        changeSpeed();
        assertEquals(getFailMsgForSpeed(1), 1, ceilingFan.getSpeed());

        changeDirection();
        assertEquals(FAILURE_MSG_FWD_DIRECTION, CeilingFan.FORWARD, ceilingFan.getDirection());
        assertEquals(getFailMsgForSpeed(1), 1, ceilingFan.getSpeed());
        changeSpeed();
        assertEquals(getFailMsgForSpeed(2), 2, ceilingFan.getSpeed());
    }

    private void changeSpeed() {
        ceilingFan.changeSpeed();
    }

    private void changeDirection() {
        ceilingFan.changeDirection();
    }

    private String getFailMsgForSpeed(int expectedSpeed) {
        return String.format(FAILURE_MSG_EXPECTED_SPEED, expectedSpeed);
    }
}
