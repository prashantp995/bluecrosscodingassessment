public class CeilingFan implements AgileFan {

    public static final String FORWARD = "Forward";
    public static final String REVERSE = "Reverse";
    public static final int OFF = 0;
    public static final int MAX_SPEED = 3;

    private int speed;
    private String direction;

    public CeilingFan() {
        speed = OFF;
        direction = FORWARD;
    }

    public int getSpeed() {
        return speed;
    }

    private void setSpeed(int speed) {
        this.speed = speed;
    }

    public String getDirection() {
        return direction;
    }

    private void setDirection(String direction) {
        this.direction = direction;
    }

    @Override
    public void changeSpeed() {
        int currentSpeed = getSpeed();
        if (currentSpeed == MAX_SPEED) {
            turnOffFan();
        } else {
            setSpeed(currentSpeed + 1);
        }
    }

    @Override
    public void changeDirection() {
        setDirection(getDirection().equals(FORWARD) ? REVERSE : FORWARD);
    }

    private void turnOffFan() {
        setSpeed(OFF);
    }

}
